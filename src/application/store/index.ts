import { createStore } from "redux";
import { reducer } from "../ducks/blockboard";

const store = createStore(reducer);

export default store;
