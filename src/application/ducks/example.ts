import { createActions } from "reduxsauce";
import { createReducer } from "reduxsauce";
import { Action } from "redux";

export enum TypesNames {
  LOGIN_ON_SUBMIT = "LOGIN_ON_SUBMIT",
  LOGIN_CHANGE_FETCH_STATUS = "LOGIN_CHANGE_FETCH_STATUS"
}

export interface IOnSubmit extends Action<TypesNames.LOGIN_ON_SUBMIT> {
  login: string;
  password: string;
}

export interface IChangeFetchStatus
  extends Action<TypesNames.LOGIN_CHANGE_FETCH_STATUS> {
  login: string;
  password: string;
}

export const { Types, Creators } = createActions<
  {
    [TypesNames.LOGIN_ON_SUBMIT]: string;
    [TypesNames.LOGIN_CHANGE_FETCH_STATUS]: string;
  },
  {
    loginOnSubmit: (login: string, password: string) => IOnSubmit;
    loginChangeFetchStatus: () => IChangeFetchStatus;
  }
>({
  loginOnSubmit: ["login", "password"],
  loginChangeFetchStatus: ["status"]
});

export interface IState {
  login: string;
  password: string;
}

type ActionTypes = IOnSubmit | IChangeFetchStatus;

const reducerTest = (state: IState, action: ActionTypes) => {
  action = action as IChangeFetchStatus;
  return state;
};

const reducerTest2 = (state: IState, action: ActionTypes) => {
  action = action as IChangeFetchStatus;
  return state;
};

export const reducer = createReducer<IState, ActionTypes>(
  {
    login: "",
    password: ""
  },
  {
    [Types.LOGIN_ON_SUBMIT]: reducerTest,
    [Types.LOGIN_CHANGE_FETCH_STATUS]: reducerTest2
  }
);
