import { createActions } from "reduxsauce";
import { createReducer } from "reduxsauce";
import { Action } from "redux";
import { BlockDrawable } from "../../model/block/BlockDrawable";
import BlockBoard from "../../model/block";
import { ConnectionInterface } from "../../model/block/BlockBoard";
import { BlockString } from "../../model/block/BlockString";
import { snakeCaseToCamelCase } from "../../model/converter/CamelCase";
import { camelCaseToSnakeCase } from "../../model/converter/SnakeCase";
import { camelCaseToSnakeUpperCase } from "../../model/converter/CompositeFuction";
import { toLowerCase } from "../../model/converter/LowerCase";

export enum TypesNames {
  ADD_BLOCK = "ADD_BLOCK",
  WRITE_BLOCK = "WRITE_BLOCK",
  MOVE_BLOCK = "MOVE_BLOCK"
}

const block1 = new BlockString("", snakeCaseToCamelCase);
block1
  .connect(new BlockString("", camelCaseToSnakeCase))
  .connect(new BlockString("", camelCaseToSnakeUpperCase))
  .connect(new BlockString("", toLowerCase))
  .connect(new BlockString("", snakeCaseToCamelCase))
  .connect(new BlockString(""));

BlockBoard.blockBoard.addBlocks(
  block1
    .getNestedBlocks()
    .map((block, i) => new BlockDrawable(block, 50, 50 + i * 210, 1300, 200))
);

const INITIAL_STATE = {
  blockById: BlockBoard.blockBoard.toJS(),
  connectionsById: BlockBoard.blockBoard.getConnectionsById()
};

export type IState = {
  blockById: { [id: number]: ReturnType<BlockDrawable["toJS"]> };
  connectionsById: { [id: number]: ConnectionInterface };
};

export const connectionsSelector = (state: IState) =>
  Object.values(state.connectionsById).map(connection => ({
    id: connection.id,
    fromPosition: {
      x:
        state.blockById[connection.fromId].position.x +
        state.blockById[connection.fromId].dimension.width / 2,
      y:
        state.blockById[connection.fromId].position.y +
        state.blockById[connection.fromId].dimension.height / 2
    },
    toPosition: {
      x:
        state.blockById[connection.toId].position.x +
        state.blockById[connection.toId].dimension.width / 2,
      y:
        state.blockById[connection.toId].position.y +
        state.blockById[connection.toId].dimension.height / 2
    }
  }));

export interface IAddBlock extends Action<TypesNames.ADD_BLOCK> {
  x: number;
  y: number;
  height: number;
  width: number;
  text: string;
}

export interface IWriteBlock extends Action<TypesNames.WRITE_BLOCK> {
  id: number;
  text: string;
}

export interface IMoveBlock extends Action<TypesNames.MOVE_BLOCK> {
  id: number;
  x: number;
  y: number;
}

export const { Types, Creators } = createActions<
  {
    [TypesNames.ADD_BLOCK]: string;
    [TypesNames.WRITE_BLOCK]: string;
    [TypesNames.MOVE_BLOCK]: string;
  },
  {
    addBlock: (
      x: number,
      y: number,
      height: number,
      width: number,
      text?: string
    ) => IAddBlock;
    writeBlock: (id: number, text: string) => IWriteBlock;
    moveBlock: (id: number, x: number, y: number) => IMoveBlock;
  }
>(
  {
    addBlock: ["x", "y", "height", "width", "text"],
    writeBlock: ["id", "text"],
    moveBlock: ["id", "x", "y"]
  },
  { prefix: "BlockBoard" }
);

type ActionTypes = IAddBlock | IWriteBlock | IMoveBlock;

const updateBlockBoard = (state: IState) => {
  const blockById = BlockBoard.blockBoard.toJS();
  const connectionsById = BlockBoard.blockBoard.getConnectionsById();
  return {
    ...state,
    blockById,
    connectionsById
  };
};

const addBlock = (state: IState, action: ActionTypes) => {
  action = action as IAddBlock;
  const { x, y, text, width, height } = action;
  BlockBoard.addDrawableStringBlock(text || "", x, y, height, width);
  const blockById = BlockBoard.blockBoard.toJS();

  return {
    ...state,
    blockById
  };
};

const writeBlock = (state: IState, action: ActionTypes) => {
  action = action as IWriteBlock;
  const { id, text } = action;

  BlockBoard.blockBoard.getBlockById(id).block.value = text;
  return updateBlockBoard(state);
};

const moveBlock = (state: IState, action: ActionTypes) => {
  action = action as IMoveBlock;
  const { id, x, y } = action;

  BlockBoard.blockBoard.getBlockById(id).setPosition(x, y);
  return updateBlockBoard(state);
};

export const reducer = createReducer<IState, ActionTypes>(INITIAL_STATE, {
  [Types[TypesNames.ADD_BLOCK]]: addBlock,
  [Types[TypesNames.WRITE_BLOCK]]: writeBlock,
  [Types[TypesNames.MOVE_BLOCK]]: moveBlock
});
