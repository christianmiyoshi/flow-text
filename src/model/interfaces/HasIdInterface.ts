export interface HasIdInterface {
  readonly id: number;
}
