import { Block } from "../block/Block";

export interface HasJsInterface {
  toJS(): any;
}

export interface HasNextBlock<T> {
  readonly nextBlocks: Block<T, any, any>[];
}
