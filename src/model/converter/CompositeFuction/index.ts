import { toUpperCase } from "../UpperCase";

import { camelCaseToSnakeCase } from "../SnakeCase";

export function camelCaseToSnakeUpperCase(text: string) {
  return toUpperCase(camelCaseToSnakeCase(text));
}
