import { camelCaseToSnakeUpperCase } from ".";

test("Camel to snake upper case", () => {
  expect(camelCaseToSnakeUpperCase("abcDef")).toBe("ABC_DEF");
});
