import { toUpperCase } from "../UpperCase";

test("To uppercase", () => {
  expect(toUpperCase("abc def")).toBe("ABC DEF");
});
