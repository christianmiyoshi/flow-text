import { toLowerCase } from ".";

test("To lowercase", () => {
  expect(toLowerCase("ABC DEF")).toBe("abc def");
});
