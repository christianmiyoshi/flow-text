export function camelCaseToSnakeCase(text: string) {
  return text
    .replace(/[\w]([A-Z])/g, (m: string) => m[0] + "_" + m[1])
    .toLowerCase();
}
