import { camelCaseToSnakeCase } from ".";

test("Camel case to snake case", () => {
  expect(camelCaseToSnakeCase("abcDef")).toBe("abc_def");
});

test("Camel case to snake case of empty", () => {
  expect(camelCaseToSnakeCase("")).toBe("");
});

test("Camel case to snake case of expression", () => {
  expect(camelCaseToSnakeCase("abc dEf")).toBe("abc d_ef");
});
