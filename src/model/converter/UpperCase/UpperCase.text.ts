import { toUpperCase } from ".";

test("To uppercase", () => {
  expect(toUpperCase("abc def")).toBe("ABC DEF");
});
