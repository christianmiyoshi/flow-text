export function snakeCaseToCamelCase(text: string) {
  return text.replace(/([-_]\w)/g, (g: string) => g[1].toUpperCase());
}
