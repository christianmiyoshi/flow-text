import { snakeCaseToCamelCase } from ".";

test("Snake case to camel case", () => {
  expect(snakeCaseToCamelCase("abc_def")).toBe("abcDef");
});

test("Snake case to camel case of empty", () => {
  expect(snakeCaseToCamelCase("")).toBe("");
});

test("Snake case to camel case of expression", () => {
  expect(snakeCaseToCamelCase("abc de_fg")).toBe("abc deFg");
});
