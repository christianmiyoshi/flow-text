import { Block } from "./Block";
import { HasIdInterface } from "../interfaces/HasIdInterface";
import { HasNextBlock } from "../interfaces/HasJsInterface";

interface Position {
  x: number;
  y: number;
}

interface Dimension {
  height: number;
  width: number;
}

export class BlockDrawable<I = any, S = any, T = any>
  implements HasIdInterface, HasNextBlock<T> {
  private _block: Block<I, S, T>;
  private _position: Position;
  private _dimension: Dimension;

  constructor(
    block: Block<I, S, T>,
    x: number,
    y: number,
    width: number,
    height: number
  ) {
    this._block = block;
    this._position = {
      x,
      y
    };
    this._dimension = { height, width };
  }

  public get id() {
    return this.block.id;
  }

  public get block() {
    return this._block;
  }

  public get position() {
    return this._position;
  }
  public setPosition(x: number, y: number) {
    this._position = { x, y };
  }

  public set x(value) {
    this.setPosition(value, this.position.y);
  }

  public get x() {
    return this.position.x;
  }

  public set y(value) {
    this.setPosition(this.position.x, value);
  }

  public get centerX() {
    return this.x + this.width / 2;
  }

  public get centerY() {
    return this.y + this.height / 2;
  }

  public get centerPosition() {
    return { x: this.centerX, y: this.centerY };
  }

  public get y() {
    return this.position.y;
  }

  public get dimension() {
    return this._dimension;
  }
  public setDimension(height: number, width: number) {
    this._dimension = { height, width };
  }

  public set height(value) {
    this.setDimension(value, this.dimension.width);
  }

  public get height() {
    return this.dimension.height;
  }

  public set width(value) {
    this.setDimension(this.dimension.height, value);
  }

  public get width() {
    return this.dimension.width;
  }

  public connect(nextBlock: Block<T, any, any>) {
    this.block.connect(nextBlock);
  }

  public toJS() {
    return {
      ...this.block.toJS(),
      position: this.position,
      dimension: this.dimension
    };
  }

  public get nextBlocks() {
    return this._block.nextBlocks;
  }
}
