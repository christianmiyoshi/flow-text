import { HasIdInterface } from "../interfaces/HasIdInterface";
import { HasJsInterface, HasNextBlock } from "../interfaces/HasJsInterface";
import { Block } from "./Block";
import { BlockDrawable } from "./BlockDrawable";
import { BlockString } from "./BlockString";

export interface ConnectionInterface {
  id: number;
  fromId: number;
  toId: number;
}

export class BlockBoard<
  T extends HasIdInterface & HasJsInterface & HasNextBlock<any>
> {
  protected _blocks: { [key: number]: T };

  constructor() {
    this._blocks = {};
  }

  public addBlock(block: T) {
    this._blocks[block.id] = block;
  }

  public addBlocks(block: T[]) {
    block.forEach(block => {
      this.addBlock(block);
    });
  }

  public get blocksById() {
    return this._blocks;
  }

  public get blocks() {
    return Object.values(this._blocks);
  }

  public getBlockById(id: number) {
    return this._blocks[id];
  }

  public deleteBlockById(id: number) {
    delete this._blocks[id];
  }

  public deleteAllBlocks() {
    this._blocks = {};
  }

  public countBlocks() {
    return Object.keys(this.blocksById).length;
  }

  public toJS() {
    return Object.values(this.blocks).reduce(
      (acc: { [id: number]: ReturnType<T["toJS"]> }, curr) => {
        acc[curr.id] = curr.toJS();
        return acc;
      },
      {}
    );
  }

  public getConnectionsById() {
    let i = 0;
    return Object.values(this.blocks).reduce(
      (acc: { [id: number]: ConnectionInterface }, block) => {
        block.nextBlocks.forEach(b => {
          acc[i] = {
            id: i,
            fromId: block.id,
            toId: b.id
          };
          i++;
        });
        return acc;
      },
      {}
    );
  }
}
