import { Block } from "../Block";

test("Simple block", () => {
  const block = new Block<number>(
    0,
    x => x + 1,
    x => x + 2
  );

  block.setInput(0);
  expect(block.getOutput()).toBe(3);
});

test("Two block", () => {
  const block1 = new Block<number>(
    0,
    x => x,
    x => x
  );

  const block2 = new Block<number>(
    0,
    x => x + 1,
    x => x + 2
  );

  block1.connect(block2);

  block1.setInput(1);
  expect(block2.getOutput()).toBe(4);
});

test("Two block with different types", () => {
  const block1 = new Block<number, number, string>(
    0,
    x => x.toString(),
    x => x
  );

  const block2 = new Block<string, number>(
    0,
    x => x,
    x => parseInt(x)
  );

  block1.addNextBlock(block2);

  block1.setInput(1);
  expect(block2.getOutput()).toBe(1);
});

test("One Block to two", () => {
  const block1 = new Block<number>(
    0,
    x => x,
    x => x
  );

  const block2 = new Block<number>(
    0,
    x => x + 1,
    x => x * 2
  );

  const block3 = new Block<number>(
    0,
    x => x + 2,
    x => x * 3
  );

  block1.addNextBlock(block2);
  block1.addNextBlock(block3);

  block1.setInput(1);
  expect(block2.getOutput()).toBe(1 * 2 + 1);
  expect(block3.getOutput()).toBe(1 * 3 + 2);
});

test("Get nested blocks", () => {
  const block1 = new Block<number>(
    0,
    x => x,
    x => x
  );
  const block2 = new Block<number>(
    0,
    x => x,
    x => x
  );
  const block3 = new Block<number>(
    0,
    x => x,
    x => x
  );

  block1.connect(block2).connect(block3);

  const nested = block1.getNestedBlocks();
  expect(nested.length).toBe(3);
  expect(nested[0]).toBe(block1);
  expect(nested[1]).toBe(block2);
  expect(nested[2]).toBe(block3);
});

test.skip("Test circular connection", () => {
  const block1 = new Block<number>(
    0,
    x => x + 1,
    x => x
  );
  const block2 = new Block<number>(
    0,
    x => x + 1,
    x => x
  );
  block1.connect(block2).connect(block1);

  block1.value = 1;
  expect(block1.value).toBe(1);
  expect(block2.value).toBe(2);
});
