import { BlockSimple } from "../BlockSimple";
import { BlockString } from "../BlockString";

describe("Test BlockString", () => {
  test("Create BlockString", () => {
    const block1 = new BlockString("0");
    expect(block1.getOutput()).toBe("0");
  });
});
