import { Block } from "../Block";
import { BlockBoard } from "../BlockBoard";
import { BlockSimple } from "../BlockSimple";

describe("Test Blockboard with blocks", () => {
  let blockBoard: BlockBoard<BlockSimple<number>>;
  let id1 = 1;
  let id2 = 2;
  let block1: BlockSimple<number>;
  let block2: BlockSimple<number>;
  beforeEach(() => {
    blockBoard = new BlockBoard();
    block1 = new BlockSimple<number>(
      0,
      x => x,
      x => x,
      id1
    );
    block2 = new BlockSimple<number>(
      0,
      x => x,
      x => x,
      id2
    );
    block1.connect(block2);

    blockBoard.addBlock(block1);
    blockBoard.addBlock(block2);
  });

  test("Test Blocks by id", () => {
    expect(blockBoard.getBlockById(id1)).toBe(block1);
    expect(blockBoard.getBlockById(id2)).toBe(block2);
  });

  test("Delete Block", () => {
    blockBoard.deleteBlockById(id1);

    expect(blockBoard.getBlockById(id1)).toBe(undefined);
    expect(blockBoard.getBlockById(id2)).toBe(block2);
  });

  test("Delete all block", () => {
    blockBoard.deleteAllBlocks();

    expect(blockBoard.getBlockById(id1)).toBe(undefined);
    expect(blockBoard.getBlockById(id2)).toBe(undefined);
  });

  test("Count blocks", () => {
    expect(blockBoard.countBlocks()).toBe(2);
  });
});
