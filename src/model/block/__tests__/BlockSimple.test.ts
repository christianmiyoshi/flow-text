import { BlockSimple } from "../BlockSimple";

describe("Test BlockSimple", () => {
  test("Create two BlockSimple", () => {
    const block1 = new BlockSimple<number>(0);
    const block2 = new BlockSimple<number>(0);
    block1.connect(block2);
    block1.setInput(1);
    expect(block2.getOutput()).toBe(1);
  });
});
