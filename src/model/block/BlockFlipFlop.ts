import { Block } from "./Block";

export class BlockFliFlop<Input> extends Block<Input, Input, Input> {
  protected _oldValue: Input;
  protected _updated: boolean;

  constructor(initialState: Input) {
    super(
      initialState,
      x => x,
      x => x
    );
    this._oldValue = initialState;
    this._updated = false;
  }

  public setInput(value: Input) {
    super.setInput(this._oldValue);
    this.oldValue = value;
  }

  private set oldValue(value: Input) {
    this._oldValue = value;
  }
}
