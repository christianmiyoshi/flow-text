import { Block } from "./Block";

export class BlockString extends Block<string> {
  constructor(
    initialString = "",
    outputFunction: (x: string) => string = x => x,
    inputFunction: (x: string) => string = x => x,
    customId?: number
  ) {
    super(initialString, outputFunction, inputFunction, customId);
  }
}
