import { Block } from "./Block";

export class BlockSimple<Input> extends Block<Input, Input, Input> {
  constructor(
    initialState: Input,
    outputFunction: (x: Input) => Input = x => x,
    inputFunction: (x: Input) => Input = x => x,
    customId?: number
  ) {
    super(initialState, outputFunction, inputFunction, customId);
  }
}
