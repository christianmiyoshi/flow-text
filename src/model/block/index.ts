import { BlockBoard } from "./BlockBoard";
import { BlockString } from "./BlockString";
import { BlockDrawable } from "./BlockDrawable";

const blockBoard = new BlockBoard<BlockDrawable<any, any, any>>();

const addDrawableStringBlock = (
  initialText: string,
  x: number,
  y: number,
  heigth: number,
  width: number
) =>
  blockBoard.addBlock(
    new BlockDrawable(new BlockString(initialText), x, y, heigth, width)
  );

const connectBlocks = (fromId: number, toId: number) => {
  const from = blockBoard.getBlockById(fromId);
  const to = blockBoard.getBlockById(toId);
  from.connect(to.block);
};

export default {
  blockBoard,
  addDrawableStringBlock,
  connectBlocks
};
