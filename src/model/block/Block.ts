import { HasIdInterface } from "../interfaces/HasIdInterface";
import { HasJsInterface, HasNextBlock } from "../interfaces/HasJsInterface";

let globalId = 0;

function getId() {
  const id = globalId;
  globalId++;
  return id;
}

export class Block<Input, State = Input, Output = State>
  implements HasIdInterface, HasJsInterface, HasNextBlock<Output> {
  protected _nextBlocks: Block<Output, any, any>[];
  protected _value: State;
  protected _inputFunction: (text: Input) => State;
  protected _outputFunction: (text: State) => Output;
  protected _id: number;

  constructor(
    initialState: State,
    outputFunction: (text: State) => Output,
    inputFunction: (text: Input) => State,
    customId: number = getId()
  ) {
    this._nextBlocks = [];
    this._value = initialState;
    this._inputFunction = inputFunction;
    this._outputFunction = outputFunction;
    this._id = customId;
  }

  setInput(text: Input) {
    this.value = this._inputFunction(text);
  }

  public get id() {
    return this._id;
  }

  public getOutput() {
    return this._outputFunction(this._value);
  }

  public addNextBlock(block: Block<Output, any, any>) {
    this._nextBlocks = [...this._nextBlocks, block];
  }

  public removeNextBlock(block: Block<any, any, any>) {
    if (this._nextBlocks.includes(block)) {
      return this._nextBlocks.filter(b => b !== block);
    }
  }

  public connect(block: Block<Output, any, any>) {
    this.addNextBlock(block);
    return block;
  }

  public getNestedBlocks(): Block<any, any, any>[] {
    return [this, ...this._nextBlocks.map(b => b.getNestedBlocks()).flat()];
  }

  public get outputFunction() {
    return this._outputFunction;
  }

  public set outputFunction(value) {
    this._outputFunction = value;
  }

  public get inputFunction() {
    return this._inputFunction;
  }

  public set inputFunction(value) {
    this._inputFunction = value;
  }

  public get value(): State {
    return this._value;
  }

  public set value(value: State) {
    this._value = value;
    this._nextBlocks.forEach(b => b.setInput(this.getOutput()));
  }

  public get nextBlocks() {
    return this._nextBlocks;
  }

  public toJS() {
    return {
      id: this.id,
      value: this.value
    };
  }
}
