import { BlockString } from "../block/BlockString";
import { camelCaseToSnakeCase } from "../converter/SnakeCase";

test("Block camel to snake case", () => {
  const block1 = new BlockString("", camelCaseToSnakeCase);
  const block2 = new BlockString();

  block1.addNextBlock(block2);
  block1.setInput("abcDef");
  expect(block2.getOutput()).toBe("abc_def");
});
