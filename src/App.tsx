import React, { useState } from "react";
import "./App.css";
import { Rnd, DraggableData, ResizableDelta } from "react-rnd";

import { Input, Button } from "antd";
import { BlockString } from "./model/block/BlockString";
import { snakeCaseToCamelCase } from "./model/converter/CamelCase";
import { camelCaseToSnakeCase } from "./model/converter/SnakeCase";
import { camelCaseToSnakeUpperCase } from "./model/converter/CompositeFuction";
import { toLowerCase } from "./model/converter/LowerCase";
import { ResizeDirection } from "re-resizable";
import { connect, useSelector, useDispatch } from "react-redux";
import {
  Types,
  TypesNames,
  Creators,
  connectionsSelector
} from "./application/ducks/blockboard";

const { TextArea } = Input;

// https://www.joshwcomeau.com/posts/dynamic-bezier-curves/
const ConnectingLine = ({
  from,
  to
}: {
  from: { x: number; y: number };
  to: { x: number; y: number };
}) => (
  <line
    x1={from.x}
    y1={from.y}
    x2={to.x}
    y2={to.y}
    stroke="rgb(255, 255, 255)"
    // strokeDasharray="5,5"
    strokeWidth={2}
  />
);

const rectangle = <rect x={40} y={15} width={30} height={65} fill="hotpink" />;
const circle = <ellipse cx={30} cy={60} rx={20} ry={20} fill="lightsalmon" />;
const triangle = <polygon points="15,80 30,55 45,80" fill="turquoise" />;

const block1 = new BlockString("", snakeCaseToCamelCase);
block1
  .connect(new BlockString("", camelCaseToSnakeCase))
  .connect(new BlockString("", camelCaseToSnakeUpperCase))
  .connect(new BlockString("", toLowerCase))
  .connect(new BlockString("", snakeCaseToCamelCase))
  .connect(new BlockString(""));
const blocks = block1.getNestedBlocks();

const titles = ["Text", "Camel", "Snake", "Upper", "Snake", "Camel"];

const App = () => {
  const [texts, setTexts] = useState(["", "", "", "", "", ""]);

  const setBlockText = (index: number, text: string) => {
    blocks[index].value = text;
    setTexts(blocks.map(b => b.value));
  };

  const [position1, setPosition1] = useState({ x: 10, y: 20 });

  const blocksProps: {
    position: any;
    dimension: any;
    id: number;
    value: string;
    //@ts-ignore
  }[] = useSelector(state => Object.values(state.blockById));

  const connections = useSelector(connectionsSelector);

  const dispatch = useDispatch();

  const [position2, setPosition2] = useState({ x: 10, y: 20 });

  return (
    <div className="App">
      <header className="App-header">
        {/* <p>Flow-text</p> */}

        {/* <Button
          onClick={() =>
            dispatch(
              Creators.addBlock(
                Math.random() * 1800,
                Math.random() * 1000,
                300,
                200
              )
            )
          }
        >
          Add block
        </Button> */}

        <svg width="3000" height="2000">
          {connections.map(c => (
            <ConnectingLine
              from={c.fromPosition}
              to={c.toPosition}
              key={c.id}
            />
          ))}
        </svg>

        {blocksProps.map((b, i) => (
          <Rnd
            size={b.dimension}
            position={b.position}
            key={b.id}
            cancel={".textarea"}
            onDrag={(e, d) => {
              dispatch(Creators.moveBlock(b.id, d.x, d.y));
            }}
          >
            <div
              style={{
                height: "100%",
                width: "100%",
                backgroundColor: "green"
              }}
            >
              <p>{titles[i]}</p>
              <TextArea
                className="textarea"
                rows={3}
                style={{ height: "50%", width: "95%", resize: "none" }}
                value={b.value}
                placeholder={"Text"}
                onChange={element => {
                  dispatch(Creators.writeBlock(b.id, element.target.value));
                }}
              />
            </div>
          </Rnd>
        ))}
        {/* <Rnd
          // default={{
          //   x: position1.x,
          //   y: position1.y,
          //   width: 320,
          //   height: 320
          // }}
          size={{ width: 100, height: 100 }}
          position={{ x: 100, y: 100 }}
          // onResize={(e, dir, refToElement, delta, position) => {
          //   setPosition1({ x: position.x, y: position.y });
          // }}
          // onDragStop={(e, d) => {
          //   this.setState({ x: d.x, y: d.y });
          // }}
          // onResizeStop={(e, direction, ref, delta, position) => {
          //   this.setState({
          //     width: ref.style.width,
          //     height: ref.style.height,
          //     ...position
          //   });
          // }}
          onDrag={(e, data) => setPosition1({ x: data.x, y: data.y })}
          cancel={".textarea"}
        >
          <div
            style={{ height: "100%", width: "100%", backgroundColor: "green" }}
          >
            <p>Text</p>
            <TextArea
              className="textarea"
              rows={3}
              style={{ height: "70%", width: "80%", resize: "none" }}
              value={texts[0]}
              placeholder={"Text"}
              onChange={element => setBlockText(0, element.target.value)}
            />
          </div>
        </Rnd> */}
        {/* 
        <Rnd
          default={{
            x: position2.x,
            y: position2.y,
            width: 320,
            height: 320
          }}
          cancel={".textarea"}
          onDrag={(e: any, data: DraggableData) =>
            setPosition2({ x: data.x, y: data.y })
          }
        >
          <div
            style={{ height: "100%", width: "100%", backgroundColor: "red" }}
          >
            <p>Camel case</p>
            <TextArea
              className="textarea"
              rows={3}
              style={{ height: "70%", width: "80%", resize: "none" }}
              value={texts[1]}
              placeholder={"Camel case"}
              onChange={element => setBlockText(1, element.target.value)}
            />
          </div>
        </Rnd> */}

        {/*

        <p>Snake case</p>
        <TextArea
          rows={3}
          value={texts[2]}
          placeholder={"Snake case"}
          onChange={element => setBlockText(2, element.target.value)}
        />

        <p>Snake upper case</p>
        <TextArea
          rows={3}
          value={texts[3]}
          placeholder={"Snake upper case"}
          onChange={element => setBlockText(3, element.target.value)}
        />

        <p>Snake case</p>
        <TextArea
          rows={3}
          value={texts[4]}
          placeholder={"Snake upper case"}
          onChange={element => setBlockText(4, element.target.value)}
        />
        <p>Camel case</p>
        <TextArea
          rows={3}
          value={texts[5]}
          placeholder={"Camel case"}
          onChange={element => setBlockText(5, element.target.value)}
        /> */}
      </header>
    </div>
  );
};

export default App;
